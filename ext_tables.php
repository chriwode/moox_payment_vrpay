<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

// Add typoscripts
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'MOOX Payment Vrpay');
?>