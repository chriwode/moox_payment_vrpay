<?php
namespace DCNGmbH\MooxPaymentVrpay\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\SingletonInterface;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Messaging\FlashMessage;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility; 
 
/**
 *
 *
 * @package moox_payment_vrpay
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class PaymentProvider implements SingletonInterface {
	
	/**
	 * objectManager
	 *
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager	
	 */
	protected $objectManager;	

	/**
	 * configurationManager
	 *
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface	
	 */
	protected $configurationManager;
	
	/**
	 * extConf
	 *
	 * @var \array
	 */
	protected $array;
	
	/**
	 * settings
	 *
	 * @var boolean
	 */
	protected $settings;
	
	
	/**
	 * Path to the locallang file
	 * @var string
	 */
	const LLPATH = 'LLL:EXT:moox_payment_vrpay/Resources/Private/Language/locallang.xlf:';
	
	/**
     *
     * @return void
     */
    public function initialize() {								
		
		// initialize object manager
		$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');		
		
		// initialize configuration manager
		$this->configurationManager = $this->objectManager->get('TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface');
		
		$this->settings = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,"MooxPaymentVrpay")['settings'];
		
		if(!empty($this->settings['testmode'])){
			if($this->settings['urls']['prepare']!=""){
				$this->settings['urls']['prepare'] = str_replace("://","://test.",$this->settings['urls']['prepare']);
			}
			if($this->settings['urls']['status']!=""){
				$this->settings['urls']['status'] = str_replace("://","://test.",$this->settings['urls']['status']);
			}
			if($this->settings['urls']['widget']!=""){
				$this->settings['urls']['widget'] = str_replace("://","://test.",$this->settings['urls']['widget']);
			}
		}
				
		// get extensions's configuration
		$this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_payment_vrpay']);
				
    }	
	
	/**
	 * get payment configuration
	 *	
	 * @return \array $paymentConfig
	 */
	public function getPaymentConfig(){
		
		// initialize
		$this->initialize();
		$paymentConfig = array();		
		
		$paymentConfig['extkey'] 	= "moox_payment_vrpay";
		$paymentConfig['extkeyUCC'] = GeneralUtility::underscoredToUpperCamelCase($paymentConfig['extkey']);
		$paymentConfig['llpath'] 	= self::LLPATH;
		$paymentConfig['title'] 	= LocalizationUtility::translate(self::LLPATH.'payment.title',$this->extensionName);
		$paymentConfig['tab'] 		= LocalizationUtility::translate(self::LLPATH.'payment.tab',$this->extensionName);
		$paymentConfig['partial'] 	= "Vrpay";
		$paymentConfig['settings']	= $this->settings;
		$paymentConfig['methods'] 	= array (
			"8a8394c35162f1e901516c394a4c2d79" => LocalizationUtility::translate(self::LLPATH.'payment.method.creditcard',$this->extensionName),
			"8a8394c45163064001518710f2780453" => LocalizationUtility::translate(self::LLPATH.'payment.method.paypal',$this->extensionName),
		);
		
		return $paymentConfig;
	}

	/**
	 * check payment configuration
	 *	
	 * @param \array $payment payment
	 * @param \array &$messages messages
	 * @param \array &$errors errors
	 * @return \boolean $valid
	 */
	public function checkPayment($payment = array(), &$messages, &$errors){
		
		$valid = false;
				
		return $valid;
		
	}
	
	/**
	 * prepare payment
	 *	
	 * @param \array $payment payment
	 * @param \array &$messages messages
	 * @param \array &$errors errors
	 * @return \array $response
	 */
	public function preparePayment($payment = array(), &$messages, &$errors){
		
		// initialize
		$this->initialize();
		
		if($this->settings['urls']['prepare']!="" && $this->settings['authentication']['userId'] && $this->settings['authentication']['password']){
			
			$return = array();
			
			$api_url 	= $this->settings['urls']['prepare'];
			$api_data 	= "authentication.userId=".$this->settings['authentication']['userId'].
						  "&authentication.password=".$this->settings['authentication']['password'].
						  "&authentication.entityId=" . $payment['vrpayentityid'] .
						  "&amount=" . $payment['amount'] .
						  "&currency=".$this->settings['currency'].
						  "&paymentType=".$this->settings['paymentType'];
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $api_url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $api_data);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			if(curl_errno($ch)) {
				return curl_error($ch);
			}
			curl_close($ch);
					
			$response = json_decode($response,TRUE);
			
			if($response['id']!=""){
				$return['vrpaycheckoutid'] = $response['id'];
			}
			
			return $return;
		
		}	else {
			
			return array();
		}
	}
	
	/**
	 * process payment
	 *	
	 * @param \array $payment payment
	 * @param \array &$messages messages
	 * @param \array &$errors errors
	 * @return \array $response
	 */
	public function processPayment($payment = array(), &$messages, &$errors){
		
		// initialize
		$this->initialize();
		
		// success response codes
		$successCodes = array(
			"000.000.000"
		);
		
		if($this->settings['urls']['status']!="" && $this->settings['authentication']['userId'] && $this->settings['authentication']['password']){
			
			$return = array();
			
			$params = \TYPO3\CMS\Core\Utility\GeneralUtility::_GET();
			
			//$checkoutId = $payment['vrpaycheckoutid'];			
			$checkoutId = isset($params["id"]) ? strip_tags(trim($params["id"])) : "";
			
			$api_url 	= str_replace("##ID##",$checkoutId,$this->settings['urls']['status']);
			$api_data 	= "?authentication.userId=".$this->settings['authentication']['userId'].
						  "&authentication.password=".$this->settings['authentication']['password'].
						  "&authentication.entityId=" . $payment['vrpayentityid'];
			
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $api_url.$api_data);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			if(curl_errno($ch)) {
				return curl_error($ch);
			}
			curl_close($ch);
			
			$response = json_decode($response,TRUE);
			$response['vrpayentityid'] = $payment['vrpayentityid'];
			$return['response'] = $response;
			if(in_array($response['result']['code'],$successCodes)){
				$return['paid'] = true;
			} else {
				$return['paid'] = false;
			}
			
			return $return;
		
		}	else {
			
			return array();
		}
	}
}
?>