<?php
########################################################################
# Extension Manager/Repository config file for ext "moox_payment_vrpay".
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
    'title' => 'MOOX Payment VR pay',
    'description' => 'Bezahlung per VR pay',
    'category' => 'plugin',
    'author' => 'Dominic Martin',
    'author_email' => 'dm@dcn.de',
    'shy' => '',
    'dependencies' => '',
    'conflicts' => '',
    'priority' => 'bottom',
    'module' => '',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 1,
    'lockType' => '',
    'author_company' => 'DCN GmbH',
    'version' => '7.0.2',
    'constraints' => array(
        'depends' => array(
            'typo3' => '6.2.3-7.9.99',
            'moox_payment' => '7.0.0-7.9.99',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    )
);
